## Updating of poseidon
1. Run the microservice locally
1. In the poseidon project, run this command in the terminal: `swagger-codegen generate -i http://localhost:8080/swagger.json -l typescript-node -o swagger-client`
1. In swagger-client/api.ts, retrieve the APIs and/or DTOs that are updated or newly added and add them into the existing respective .ts files
1. Replace all http.ClientResponse to http.IncomingMessage

Swagger Codegen 2.4.13